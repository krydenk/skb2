﻿
cors = require('cors');
express = require('express');

var app = express();
app.use(cors());

app.listen(3002, function () {
	console.log('Server running at http://127.0.0.1:3002/');
});


app.get('/', function (req, res) {
	var fullname = req.query.fullname ? req.query.fullname : 0;
	
	if (fullname == '') matchesOfSpace = 10; 
	
	else {
		
		var matchesOfSpace = 0;
		var pos = 0;
		while (true) {
			var foundPos = fullname.indexOf(' ', pos);
			if (foundPos !== -1) {
				pos = foundPos + 1;
				matchesOfSpace++;
			}
			else break;
		}
	}
	
	if (matchesOfSpace == 0) 
		result = fullname;
	else if (matchesOfSpace == 1)
		result = fullname.replace(/([a-za-я])[a-za-я]* ([a-za-я]*)/i, '$2 $1.');
	else if (matchesOfSpace == 2)
		result = fullname.replace(/([a-za-я])[a-za-я]* ([a-za-я])[a-za-я]* ([a-za-я]*)/i, '$3 $1. $2.');
	else
		result = 'Invalid fullname';
	
	res.send(result.toString());
  
});